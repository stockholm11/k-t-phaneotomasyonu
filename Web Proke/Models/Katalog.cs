//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web_Proke.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Katalog
    {
        public int KatalogID { get; set; }
        public string Adı { get; set; }
        public string KonuBasliklari { get; set; }
        public string Tür { get; set; }
        public string YayinBilgisi { get; set; }
        public string FizikselNitelik { get; set; }
        public Nullable<int> DolapKodu { get; set; }
        public Nullable<int> AdminID { get; set; }
        public string ISBN_ISSN { get; set; }
    
        public virtual Admin Admin { get; set; }
        public virtual Konum Konum { get; set; }
    }
}
