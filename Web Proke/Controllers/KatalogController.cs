﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{
    public class KatalogController : Controller
    {
        private KutuphaneEntities4 db = new KutuphaneEntities4();

        // GET: Katalog
        public ActionResult Kitaplar()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Kitap");
            return View(katalog.ToList());
        }

        // GET: Katalog/Details/5
        public ActionResult Ansiklopediler(int? id)
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Ansiklopedi");
            return View(katalog.ToList());
        }

        // GET: Katalog/Create
        public ActionResult Makaleler()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Makale");
            return View(katalog.ToList());
        }

        // POST: Katalog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KatalogID,Adı,KonuBasliklari,Tür,YayinBilgisi,FizikselNitelik,DolapKodu,AdminID,ISBN_ISSN")] Katalog katalog)
        {
            if (ModelState.IsValid)
            {
                db.Katalog.Add(katalog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", katalog.AdminID);
            ViewBag.DolapKodu = new SelectList(db.Konum, "DolapKodu", "Durum", katalog.DolapKodu);
            return View(katalog);
        }

        // GET: Katalog/Edit/5
        public ActionResult Dergiler()
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Dergi");
            return View(katalog.ToList());
        }

        // POST: Katalog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KatalogID,Adı,KonuBasliklari,Tür,YayinBilgisi,FizikselNitelik,DolapKodu,AdminID,ISBN_ISSN")] Katalog katalog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(katalog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", katalog.AdminID);
            ViewBag.DolapKodu = new SelectList(db.Konum, "DolapKodu", "Durum", katalog.DolapKodu);
            return View(katalog);
        }

        // GET: Katalog/Delete/5
        public ActionResult DigitalKayitlar(int? id)
        {
            var katalog = db.Katalog.Where(x => x.Tür == "Digital Kayit");
            return View(katalog.ToList());
        }

        // POST: Katalog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Katalog katalog = db.Katalog.Find(id);
            db.Katalog.Remove(katalog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
