﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{
    public class CalisanlarController : Controller
    {
        private KutuphaneEntities4 db = new KutuphaneEntities4();

        // GET: Calisanlar
        public ActionResult Index()
        {
            var calisanlar = db.Calisanlar.Include(c => c.Admin);
            return View(calisanlar.ToList());
        }

        // GET: Calisanlar/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisanlar calisanlar = db.Calisanlar.Find(id);
            if (calisanlar == null)
            {
                return HttpNotFound();
            }
            return View(calisanlar);
        }

        // GET: Calisanlar/Create
        public ActionResult Create()
        {
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi");
            return View();
        }

        // POST: Calisanlar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CalisanID,CalisanAdi,CalisanSoyadi,e_posta,Telefon,Ünvani,AdminID")] Calisanlar calisanlar)
        {
            if (ModelState.IsValid)
            {
                db.Calisanlar.Add(calisanlar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", calisanlar.AdminID);
            return View(calisanlar);
        }

        // GET: Calisanlar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisanlar calisanlar = db.Calisanlar.Find(id);
            if (calisanlar == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", calisanlar.AdminID);
            return View(calisanlar);
        }

        // POST: Calisanlar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CalisanID,CalisanAdi,CalisanSoyadi,e_posta,Telefon,Ünvani,AdminID")] Calisanlar calisanlar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(calisanlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", calisanlar.AdminID);
            return View(calisanlar);
        }

        // GET: Calisanlar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calisanlar calisanlar = db.Calisanlar.Find(id);
            if (calisanlar == null)
            {
                return HttpNotFound();
            }
            return View(calisanlar);
        }

        // POST: Calisanlar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Calisanlar calisanlar = db.Calisanlar.Find(id);
            db.Calisanlar.Remove(calisanlar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
