﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{
    public class KonumController : Controller
    {
        private KutuphaneEntities4 db = new KutuphaneEntities4();

        // GET: Konums
        public ActionResult Index()
        {
            var konum = db.Konum.Include(k => k.Admin);
            return View(konum.ToList());
        }

        // GET: Konums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konum konum = db.Konum.Find(id);
            if (konum == null)
            {
                return HttpNotFound();
            }
            return View(konum);
        }

        // GET: Konums/Create
        public ActionResult Create()
        {
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi");
            return View();
        }

        // POST: Konums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DolapKodu,RafNo,RafSira,Durum,AdminID")] Konum konum)
        {
            if (ModelState.IsValid)
            {
                db.Konum.Add(konum);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", konum.AdminID);
            return View(konum);
        }

        // GET: Konums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konum konum = db.Konum.Find(id);
            if (konum == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", konum.AdminID);
            return View(konum);
        }

        // POST: Konums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DolapKodu,RafNo,RafSira,Durum,AdminID")] Konum konum)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", konum.AdminID);
            return View(konum);
        }

        // GET: Konums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konum konum = db.Konum.Find(id);
            if (konum == null)
            {
                return HttpNotFound();
            }
            return View(konum);
        }

        // POST: Konums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Konum konum = db.Konum.Find(id);
            db.Konum.Remove(konum);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
