﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Proke.Models;

namespace Web_Proke.Controllers
{
    public class ÜyelerController : Controller
    {
        private KutuphaneEntities4 db = new KutuphaneEntities4();

        // GET: Üyeler
        public ActionResult Index()
        {
            var üyeler = db.Üyeler.Include(ü => ü.Admin);
            return View(üyeler.ToList());
        }

        // GET: Üyeler/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Üyeler üyeler = db.Üyeler.Find(id);
            if (üyeler == null)
            {
                return HttpNotFound();
            }
            return View(üyeler);
        }

        // GET: Üyeler/Create
        public ActionResult Create()
        {
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi");
            return View();
        }

        // POST: Üyeler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ÜyeID,ÜyeAdiSoyadi,TelefonNo,KayitTarihi,a_mail,KullanıcıAdi,Parola,AdminID")] Üyeler üyeler)
        {
            if (ModelState.IsValid)
            {
                db.Üyeler.Add(üyeler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", üyeler.AdminID);
            return View(üyeler);
        }

        // GET: Üyeler/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Üyeler üyeler = db.Üyeler.Find(id);
            if (üyeler == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", üyeler.AdminID);
            return View(üyeler);
        }

        // POST: Üyeler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ÜyeID,ÜyeAdiSoyadi,TelefonNo,KayitTarihi,a_mail,KullanıcıAdi,Parola,AdminID")] Üyeler üyeler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(üyeler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdminID = new SelectList(db.Admin, "AdminID", "KullaniciAdi", üyeler.AdminID);
            return View(üyeler);
        }

        // GET: Üyeler/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Üyeler üyeler = db.Üyeler.Find(id);
            if (üyeler == null)
            {
                return HttpNotFound();
            }
            return View(üyeler);
        }

        // POST: Üyeler/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Üyeler üyeler = db.Üyeler.Find(id);
            db.Üyeler.Remove(üyeler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
