﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Web_Proke.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Services()
        {
            return View();
        }
        public ActionResult Portfilo()
        {
            return View();
        }
        public ActionResult Kitaplar()
        {
            
            return View();
        }

        public ActionResult Pricing()
        {
            return View();
        }
        public ActionResult Blog()
        {
            return View();
        }
    }
}